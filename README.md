## dream2ltexx-user 8.0.0 R16NW G955FXXU1CRC7 release-keys
- Manufacturer: samsung
- Platform: exynos5
- Codename: dream2lte
- Brand: samsung
- Flavor: lineage_dream2lte-userdebug
- Release Version: 12
- Id: SP2A.220505.002
- Incremental: eng.ivanme.20220511.044826
- Tags: test-keys
- CPU Abilist: 
- A/B Device: false
- Locale: en-US
- Screen Density: 480
- Fingerprint: samsung/dream2ltexx/dream2lte:8.0.0/R16NW/G955FXXU1CRC7:user/release-keys
- OTA version: 
- Branch: dream2ltexx-user-8.0.0-R16NW-G955FXXU1CRC7-release-keys-random-text-80762307323877
- Repo: samsung_dream2lte_dump


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
