#!/vendor/bin/sh
if ! applypatch --check EMMC:/dev/block/platform/11120000.ufs/by-name/RECOVERY$(getprop ro.boot.slot_suffix):40851472:70189de0af768a2fbf2d3487ffee8bc08fbe9edd; then
  applypatch --bonus /vendor/etc/recovery-resource.dat \
          --patch /vendor/recovery-from-boot.p \
          --source EMMC:/dev/block/platform/11120000.ufs/by-name/BOOT$(getprop ro.boot.slot_suffix):32065552:94c49fc3ca5cdf0e61a925d8ebfef3a099310aae \
          --target EMMC:/dev/block/platform/11120000.ufs/by-name/RECOVERY$(getprop ro.boot.slot_suffix):40851472:70189de0af768a2fbf2d3487ffee8bc08fbe9edd && \
      log -t recovery "Installing new recovery image: succeeded" || \
      log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
